"""
permutations.py
this is a lean library to wrok with permutations, represented by a list of index
(so a list of length n would have all the integer between 0 and n-1 )
"""
import math

#utility
def is_permutation(permu):
	"""
	determine if a list is a permutation in the format of this Library
	do not change the argument (a shallow copy is used, wich is sufficent here)
	"""
	
	permu_copy = permu.copy()
	permu_copy.sort()
	for i,elem in enumerate(permu_copy):
		if(elem != i):
			return False
	
	return True

def complete_permutation(permu_stub, permu_graft):
	"""
	complete in place the list permu_stub 
	(wich represent the stub of a permutation where holes are noted -1)
	with permu_graft, representing a permutation of
	[len(permu_stub) -(permu_graft),len(permu_stub)] , to form a permutation.
	If permu_graft is shorter than the number of holes, the function behavior is undefined
	(right now it'll unelegantly crash), longer the unused number are ignored.
	"""
	
	graft_index = 0
	for i, elem in enumerate(permu_stub):
		if elem == -1:
			permu_stub[i] = permu_graft[graft_index]
			graft_index +=1
	return permu_stub


def invert_permutation(permu):
	"""
	invert the given premutation (doesn't change the input)
	"""
	
	res = [-1]*len(permu)
	
	for index, elem in enumerate(permu):
		
		res[elem] = index
	
	return res


#hash functions without parameter

def permu_factorial_factor(num, n):
	"""
	give the permutation of Sigma(n) associated with the number num by the factorial
	factor method
	"""
	res = [-1]*n #shall become the awaited permutation
		
	q_k = num % math.factorial(n)
	
	for k in range(n, 0, -1): #put out k-1 in the permutation
		#print("k= ",k)#DEV
		q_k, rem = divmod(q_k,k) #quotient and remainder
		
		i = 0 #index of k
		while(i <= rem): #scrolling through res to find the place where k ought to be, overshooting because we need to find the first open postion
			if(res[i] != -1):
				rem +=1
			i+=1
		
		#print("i-1= ",i-1)#DEV
		res[i-1] = k-1
	
# 	for i in range(n):
# 		if(res[i] == -1):
# 				res[i] = 0
# 				break
	
	return res

def backward_permu_factorial_factor(num, n):
	"""
	give the permutation of Sigma(n) associated with the number num by the backward 
	factorial factor method
	"""
	res = [-1]*n #shall become the awaited permutation
		
	q_k = num % math.factorial(n)
	
	for k in range(n, 0, -1): #put out k-1 in the permutation
		q_k, rem = divmod(q_k,k) #quotient and remainder
		
		i = 0 #index of k
		while(i <= rem): #scrolling through res to find the place where k ought to be, overshooting because we need to find the first open postion
			if(res[i] != -1):
				rem +=1
			i+=1
		
		res[i-1] = n-k
	
	return res		

def double_hash_prime_mod(num, n):
	"""
	give the permutation associated with the double hash method, with  x -> x mod n and
	x -> (x mod (n-1)) +1 as the respective hash functions, so can only be used if n is
	prime
	"""
	res = [num % n]
	
	incr = (num % (n-1)) +1
	
	for i in range(1,n):
		res.append((res[-1] + incr)%n)
	
	return res

def double_hash_power_mod(num, n):
	"""
	give the permutation associated with the double hash method, with  x -> x mod n and
	x -> (x mod (n-1)) +1 + (x %2) as the respective hash functions, so can only be used
	if n is a power of 2
	"""

	res = [num % n]
		
	incr = (num % (n-1)) +1 + ((num % (n-1)) %2)
		
	for i in range(1,n):
		res.append((res[-1] + incr)%n)
	
	return res


def factorial_base(num, n):
	"""
	give the permutation of Sigma(n) associated with the number num by the factorial base
	method
	"""
	res = [-1]*n #shall become the awaited permutation
	
	d_k = math.factorial(n)
	
	r_k = num % d_k
	
	for k in range(n, 0, -1):
	
		d_k /= k
	
		qot, r_k = divmod(r_k,d_k) #quotient and remainder
		
		i = 0 #index of k
		while(i <= qot): #scrolling through res to find the place where k ought to be, overshooting because we need to find the first open postion
			if(res[i] != -1):
				qot +=1
			i+=1
		
		res[i-1] = k-1

	return res

def backward_factorial_base(num, n):
	"""
	give the permutation of Sigma(n) associated with the number num by the backward 
	factorial base method
	"""
	res = [-1]*n #shall become the awaited permutation
	
	d_k = math.factorial(n)
	
	r_k = num % d_k
	
	for k in range(n, 0, -1):
	
		d_k /= k
	
		qot, r_k = divmod(r_k,d_k) #quotient and remainder
		
		i = 0 #index of k
		while(i <= qot): #scrolling through res to find the place where k ought to be, overshooting because we need to find the first open postion
			if(res[i] != -1):
				qot +=1
			i+=1
		
		res[i-1] = n-k

	return res	
	
#hash functions with parameter



def inverted_partial_permu_factorial_factor(cut, other_hash, num, n):
	"""
	give the permutation of Sigma(n) associated with the number num by the inverted 
	partial factorial factor method, where cut is the number from wich point on we switch
	to the other_hash hashing function without parameter (a double hash for exemple)
	"""
	
	return invert_permutation(partial_permu_factorial_factor(cut, other_hash, num, n))	

def partial_permu_factorial_factor(cut, other_hash, num, n):
	"""
	give the permutation of Sigma(n) associated with the number num by the partial 
	factorial factor method, where cut is the number from wich point on we switch to 
	the other_hash hashing function without parameter (a double hash for exemple)
	"""
	res = [-1]*n #shall become the awaited permutation
		
	q_k = num % math.factorial(n)
	
	for k in range(n, cut, -1): #put out k-1 in the permutation
		q_k, rem = divmod(q_k,k) #quotient and remainder
		
		i = 0 #index of k
		while(i <= rem): #scrolling through res to find the place where k ought to be, overshooting because we need to find the first open postion
			if(res[i] != -1):
				rem +=1
			i+=1
		
		res[i-1] = n-k
	
	permu_graft_element = other_hash(q_k,cut)
	
	permu_graft = [ i + n - cut for i in permu_graft_element ]
	
	complete_permutation(res, permu_graft)
	
	return res













