"""
graphpermutationrun.py

Invoque with: python3 path_to_this_script


Positional (mandatory) argument:


Keyword (optional) arguments:


Showing the results of some experiences on the different permutation. Needs the
output of "permutationrun.py" in its cannonical position.
"""

import os
import sys
import argparse
import random
import secrets	
import copy
import math
#import csv
#import configparser
import functools

import pandas


#import permutations
import graphspermutation


# Constants definitions

outputs_top_storage_name = "Outputs"

pickles_storage_name = "Pickles"

output_exention = "pdf"

pickle_exention = "pkl"

colors = ['blue', 'red', 'green', 'purple', 'orange', 'brown']

#input_top_folder_name = "permutationrun"


# Utility objects and functions

# def test_compared():
# 	
# 	lis_perm = ["double_hash_power_mod", "permu_factorial_factor"]
# 	
# 	run_output_path = os.path.join(common_path, "Outputs", "permutationrun")
# 	
# 	table_size = 4
# 	num_size = 24
# 	seed = 12591348937789221142
# 	num_exemple = 100
# 	
# 	table_size_list = [4,8]
# 	num_size_list = [24,40320]
# 	
# 	compared_total(lis_perm, run_output_path, outputs_path, table_size_list, num_size_list, seed, num_exemple, colors)

# CLI exploitation

common_path = os.path.dirname(sys.argv[0])

this_script_name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

parser = argparse.ArgumentParser(description = "Showing the results of some "+
	"experiments on the different permutation. Needs the output of "+
	"'permutationrun.py' in its cannonical position.")

args = parser.parse_args()


# Check output storage, and perhaps create it

project_script_outputs_path = os.path.join(common_path, outputs_top_storage_name)

if not os.path.exists(project_script_outputs_path):
	os.mkdir(project_script_outputs_path)

script_outputs_path = os.path.join(project_script_outputs_path, this_script_name)

if not os.path.exists(script_outputs_path):
	os.mkdir(script_outputs_path)


# Check pickles folder

pickles_storage_path = os.path.join(common_path, pickles_storage_name)

if not os.path.exists(pickles_storage_path):
	
	raise RuntimeError("Experimental data not found!")


# Schmurf

the_global_color = {}

# Iterate over the table size

tables_size_folder_listing_generator = os.walk(pickles_storage_path)

same_as_permutationrun_output_folder, table_size_folders, files =\
	next(tables_size_folder_listing_generator)

for a_table_size_folder in table_size_folders:
	
	table_size = int(a_table_size_folder.removeprefix("table size "))
	
	table_folder_path = os.path.join(pickles_storage_path, a_table_size_folder)
	
	# Iterate over the num size
	
	num_size_folder_listing_generator = os.walk(table_folder_path)
	
	same_as_table_folder_path, num_size_folders, files =\
		next(num_size_folder_listing_generator)
	
	for a_num_size_folder in num_size_folders:
		
		num_size = int(a_num_size_folder.removeprefix("num size "))
		
		num_size_folder_path = os.path.join(table_folder_path, a_num_size_folder)
		
		# Iterate over the seed
		
		seed_folder_listing_generator = os.walk(num_size_folder_path)
		
		same_as_num_size_folder_path, seed_folders, files =\
			next(seed_folder_listing_generator)
		
		for a_seed_folder in seed_folders:
			
			seed = int(a_seed_folder.removeprefix("seed "))
			
			seed_folder_path = os.path.join(num_size_folder_path, a_seed_folder)
			
			# Get the experimental data
			
			experiments_listing_generator = os.walk(seed_folder_path)
			
			same_as_seed_folder_path, whatever, experimental_files =\
				next(experiments_listing_generator)
			
			the_data = {}
			the_color = {}
			
			for a_file in sorted(experimental_files):	# dictionaries keeps insertion order!
				
				the_in_path = os.path.join(seed_folder_path, a_file)
				
				(the_tag, the_type) = os.path.splitext(a_file)
				
				if (the_type != "."+pickle_exention) or (the_tag == "elements"):
					
					continue
				
				if the_tag not in the_global_color:
					
					the_global_color[the_tag] = colors.pop(0)
				
				the_color[the_tag] = the_global_color[the_tag]
				
				the_data[the_tag] = pandas.read_pickle(the_in_path)
				
			# Make pretty pictures
			
			graphspermutation.compared_lines(script_outputs_path, table_size,
				num_size, seed, the_color, the_data)
			
			graphspermutation.compared_lines_bis(script_outputs_path, table_size,
				num_size, seed, the_color, the_data)
			
			#graphspermutation.compared_bars(script_outputs_path, table_size,
			#	num_size, seed, the_color, the_data)
			
			#graphspermutation.compared_histograms(script_outputs_path, table_size,
			#	num_size, seed, the_color, the_data)
			
			#graphspermutation.compared_histograms_bis(script_outputs_path, table_size,
			#	num_size, seed, the_color, the_data)
			
			graphspermutation.compared_histograms_ter(script_outputs_path, table_size,
				num_size, seed, the_color, the_data)
			
			graphspermutation.compared_boxes(script_outputs_path, table_size,
				num_size, seed, the_color, the_data)



# In the end, there can be None

sys.exit(0)
