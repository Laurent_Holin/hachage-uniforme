"""
graphspermutation.py

exploitation of the data produced by permutationrun
"""

import os
import csv

import numpy

import pandas

import matplotlib
import matplotlib.figure
import matplotlib.backends.backend_pdf
import matplotlib.gridspec

from utilities import *


# Constants definitions

inch = 2.54

fig_extension = "pdf"


# Utility objects and functions

def labelificator(packaged):
	
	if '+' in packaged:
		
		return packaged.replace('+', ' ')
	
	else:
		
		return packaged


# def in_path_builder(run_output_path,table_size,num_size,seed): #ATESTER
# 	return os.path.join(run_output_path, "table size "+str(table_size),
#  		"num size "+str(num_size), "seed "+str(seed))


# Plots

def compared_lines(outputs_path, table_size, num_size, seed, the_color, the_data):
	"""
	draw the line chart of the number of collision of the insertion of the k-th element,
	for each permutation in lis_perm
	"""
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "lines"+"."+fig_extension)
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	slots = numpy.arange(table_size)
	
	labels = [str(toto) for toto in slots]
	
	how_many_perms = len(the_data)
	
	nominal_width = 1/(2*how_many_perms)
	
	layout = matplotlib.gridspec.GridSpec(1,1)
	
	the_plot = the_figure.add_subplot(layout[0,0])	
	
	for a_perm in the_data:
		
		the_plot.plot(the_data[a_perm].mean(axis = 0), color = the_color[a_perm],
			label = labelificator(a_perm))
	
	the_plot.set_xlim([nominal_width/12-1/2, table_size+nominal_width/12-1/2])
	
	the_plot.set_xticks(slots)
	
	the_plot.set_xticklabels(labels)
	
	the_plot.set_xlabel("Ordre d'insertion")
	
	the_plot.set_ylabel("Nombre de collisions pour cette insertion")
	
	the_plot.legend(loc = 'upper left')
	
	the_plot.grid(axis = 'both')
	
	the_figure.suptitle("Nombre moyen de collisions dans l'expérience", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)


def compared_lines_bis(outputs_path, table_size, num_size, seed, the_color, the_data):
	"""
	draw the line chart of the number of collision of the insertion of the k-th element,
	for each permutation in lis_perm
	"""
	
	#the_out_path = out_path_builder(outputs_path, table_size, num_size, seed, "linesvar")
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "linesvar"+"."+fig_extension)
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	slots = numpy.arange(table_size)
	
	labels = [str(toto) for toto in slots]
	
	how_many_perms = len(the_data)
	
	nominal_width = 1/(2*how_many_perms)
	
	layout = matplotlib.gridspec.GridSpec(1,1)
	
	the_plot = the_figure.add_subplot(layout[0,0])	
	
	for a_perm in the_data:
		
		#the_plot.plot(the_data[a_perm].mean(axis = 0), color = the_color[a_perm],
		#	label = labelificator(a_perm))
		#print(the_data[a_perm].std(axis = 0))
		the_plot.errorbar(slots, the_data[a_perm].mean(axis = 0),
			yerr = the_data[a_perm].std(axis = 0), linestyle = '-',
			capsize = 10, markeredgecolor = the_color[a_perm],
			markeredgewidth = 3,
			color = the_color[a_perm], label = labelificator(a_perm))
	
	the_plot.set_xlim([nominal_width/12-1/2, table_size+nominal_width/12-1/2])
	
	the_plot.set_xticks(slots)
	
	the_plot.set_xticklabels(labels)
	
	the_plot.set_xlabel("Ordre d'insertion")
	
	the_plot.set_ylabel("Nombre de collisions pour cette insertion")
	
	the_plot.legend(loc = 'upper left')
	
	the_plot.grid(axis = 'both')
	
	the_figure.suptitle("Nombre moyen de collisions dans l'expérience", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)
	
	
# def compared_lines_all_seed(lis_perm, run_output_path, outputs_path, table_size, num_size, seed, num_exemple, colors): #TODO
# 	"""
# 	same as compared_lines but sum on all the seed
# 	"""
# 	pass#TODO
	

def compared_bars(outputs_path, table_size, num_size, seed, the_color, the_data):
	
	#the_out_path = out_path_builder(outputs_path, table_size, num_size, seed, "bars")
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "bars"+"."+fig_extension)
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	how_many_perms = len(the_data)
	
	slots = numpy.arange(table_size)
	
	labels = [str(toto) for toto in slots]
	
	nominal_width = 1/(2*how_many_perms)
	
	layout = matplotlib.gridspec.GridSpec(1,1)
	
	the_plot = the_figure.add_subplot(layout[0,0])
	
	pidx = 0
	
	for a_perm in the_data:
		
		the_plot.bar(slots+nominal_width*(pidx-how_many_perms+1+(how_many_perms%2)/2),
			the_data[a_perm].sum(axis = 0), nominal_width,
			color = the_color[a_perm], align = 'edge', label = labelificator(a_perm))
		
		pidx += 1
	
	the_plot.set_xlim([nominal_width/12-1/2, table_size+nominal_width/12-1/2])
	
	the_plot.set_xticks(slots)
	
	the_plot.set_xticklabels(labels)
	
	the_plot.set_xlabel("Ordre d'insertion")
	
	the_plot.set_ylabel("Nombre de collisions pour cette insertion")
	
	the_plot.grid(axis = 'y')
	
	the_plot.legend(loc = 'upper left')
	
	the_figure.suptitle("Nombre total de collisions dans l'expérience", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)


def compared_histograms(outputs_path, table_size, num_size, seed, the_color, the_data):
	
	#the_out_path = out_path_builder(outputs_path, table_size, num_size, seed, "histos")
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "histos"+"."+fig_extension)
	
	how_many_perms = len(the_data)
	
	slots = numpy.arange(table_size)
	
	labels = [str(toto) for toto in slots]
	
	nominal_width = 1/(2*how_many_perms)
	
# 	experimental_data = {}
# 	
# 	for a_perm in lis_perm:
# 		
# 		the_in_path = os.path.join(common_in_path, a_perm+".csv")
# 		
# 		experimental_data[a_perm] = pandas.read_csv(the_in_path, header = None,
# 			delimiter = ';')
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	max_hists_by_columns = 8
	
	nb_lines_of_hists = table_size//max_hists_by_columns
	nb_lines_of_hists += (0 if (nb_lines_of_hists*max_hists_by_columns == table_size) else 1)
	
	nb_columns_of_hists = min(table_size, max_hists_by_columns)
	
	layout = matplotlib.gridspec.GridSpec(5*nb_lines_of_hists+1, nb_columns_of_hists)
	
	if nb_lines_of_hists > 1:
		
		layout.update(left = 0.0425, right = 0.99, top = 0.925, bottom = 0.025, hspace = 20.0)
	
	else:
		
		layout.update(left = 0.075, right = 0.95, bottom = 0.05)
	
	fuzz = 1.1
	
	for sidx in range(table_size):
		
		(n_l, n_c) = divmod(sidx, max_hists_by_columns)
		
		the_plot = the_figure.add_subplot(layout[5*n_l:5*n_l+5, n_c])
		
		#for a_perm in the_data:
		#	
		#	print(the_data[a_perm][sidx])
		
		#for pidx in range(how_many_perms):
		#	
		#	print(experimental_data[lis_perm[pidx]][sidx])
		
# 		x = numpy.transpose(numpy.stack([experimental_data[lis_perm[pidx]][sidx] for pidx in range(how_many_perms)]))
		
		x = numpy.transpose(numpy.stack([the_data[a_perm][sidx] for a_perm in the_data]))
		
		(n, bins, patches) = the_plot.hist(x, bins = sidx+1, align = 'mid', color = the_color.values())
		
		colisions = [(bins[toto]+bins[toto+1])/2 for toto in range(len(bins)-1)]
		
		the_plot.set_xticks(colisions)
		
		the_plot.set_xticklabels(range(sidx+1), fontsize = ('xx-small' if (nb_lines_of_hists > 1) else 'medium'))
		
		the_plot.set_xlabel("Nombre de collisions")
		
		#the_plot.set_ylim([0, num_exemple*fuzz])
		
		the_plot.grid(axis = 'y')
		
		the_plot.set_title("Ordre d'insertion: "+str(sidx), fontsize = ('small' if (nb_lines_of_hists > 1) else 'medium'))
	
	layout.update(top = 0.15, bottom = 0.05)
	
	the_plot = the_figure.add_subplot(layout[5*nb_lines_of_hists, 0:nb_columns_of_hists])
	
	the_plot.axis('off')
	
	am_I_a_legend = the_plot.legend([matplotlib.patches.Rectangle((0, 0), 1, 1, fc = the_color[a_perm])
		for a_perm in the_data], [labelificator(a_perm) for a_perm in the_data],
		ncol = how_many_perms)
	
	the_figure.suptitle("Occurences du nombre de collisions", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)


def compared_histograms_bis(outputs_path, table_size, num_size, seed, the_color, the_data):
	
	#the_out_path = out_path_builder(outputs_path, table_size, num_size, seed, "barhistos")
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "lines"+"."+fig_extension)
	
	how_many_perms = len(the_data)
	
	slots = numpy.arange(table_size)
	
	labels = [str(toto) for toto in slots]
	
	nominal_width = 1/(2*how_many_perms)
	
	by_perm_and_collisions = {}
	
	for a_perm in the_data:
		
		by_perm_and_collisions[a_perm] = {}
		
		for cidx in range(table_size):
			
			by_perm_and_collisions[a_perm][cidx] = []
			
			for sidx in range(table_size):
				
				try:
					
					nb = pandas.Series.value_counts(the_data[a_perm][sidx])[cidx]
				
				except:
					
					nb = 0
				
				by_perm_and_collisions[a_perm][cidx].append(nb)
	
	#print(by_perm_and_collisions)
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	max_hists_by_columns = 8
	
	nb_lines_of_hists = table_size//max_hists_by_columns
	nb_lines_of_hists += (0 if (nb_lines_of_hists*max_hists_by_columns == table_size) else 1)
	
	nb_columns_of_hists = min(table_size, max_hists_by_columns)
	
	layout = matplotlib.gridspec.GridSpec(5*nb_lines_of_hists+1, nb_columns_of_hists)
	
	if nb_lines_of_hists > 1:
		
		layout.update(left = 0.0425, right = 0.99, top = 0.925, bottom = 0.025, hspace = 20.0)
	
	else:
		
		layout.update(left = 0.075, right = 0.95, bottom = 0.05)
	
	fuzz = 1.1
	
	for cidx in range(table_size):
		
		(n_l, n_c) = divmod(cidx, max_hists_by_columns)
		
		the_plot = the_figure.add_subplot(layout[5*n_l:5*n_l+5, n_c])
		
		pidx = 0
		
		for a_perm in the_data:
				
			the_plot.bar(slots+nominal_width*(pidx-how_many_perms+1+(how_many_perms%2)/2),
				by_perm_and_collisions[a_perm][cidx], nominal_width,
				color = the_color[a_perm], align = 'edge')
			
			pidx += 1
		
		the_plot.set_xlim([nominal_width/12-1/2, table_size+nominal_width/12-1/2])
	
		the_plot.set_xticks(slots)
	
		the_plot.set_xticklabels(labels, fontsize = ('xx-small' if (nb_lines_of_hists > 1) else 'medium'))
		
		the_plot.set_xlabel("Ordre d'insertion")
		
		the_plot.set_ylim([0, len(the_data[list(the_data.keys())[0]])*fuzz])
		
		the_plot.grid(axis = 'y')
		
		if cidx == 0:
			
			the_title = "Aucune collision"
		
		elif cidx == 1:
			
			the_title = "Une collision"
		
		else:
			
			the_title = str(cidx)+" collisions"
		
		the_plot.set_title(the_title, fontsize = ('small' if (nb_lines_of_hists > 1) else 'medium'))
	
	layout.update(top = 0.15, bottom = 0.05)
	
	the_plot = the_figure.add_subplot(layout[5*nb_lines_of_hists, 0:nb_columns_of_hists])
	
	the_plot.axis('off')
	
	am_I_a_legend = the_plot.legend([matplotlib.patches.Rectangle((0, 0), 1, 1, fc = the_color[a_perm])
		for a_perm in the_data], [labelificator(a_perm) for a_perm in the_data],
		ncol = how_many_perms)
	
	the_figure.suptitle("Occurences du nombre de collisions par ordre d'insertion", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)


def compared_histograms_ter(outputs_path, table_size, num_size, seed, the_color, the_data):
	
	#the_out_path = out_path_builder(outputs_path, table_size, num_size, seed, "spreads")
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "spreads"+"."+fig_extension)
	
	#how_many_perms = len(the_data)
	
	#nominal_width = 1/(2*how_many_perms)
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	layout = matplotlib.gridspec.GridSpec(1,1)
	
	the_plot = the_figure.add_subplot(layout[0,0])
	
	x = numpy.transpose(numpy.stack([the_data[a_perm].sum(axis = 1) for a_perm in the_data]))
	
	the_means = x.mean(axis = 0)
	
	the_stds = x.std(axis = 0)
	
	the_labels = [labelificator(a_perm) for a_perm in the_data]
	
	for lidx in range(len(the_labels)):
		
		the_labels[lidx] += " "+"$(\overline{x} = $"+f"{the_means[lidx]:.3}"+\
			"$, \sigma = $"+f"{the_stds[lidx]:.3}"+"$)$"
	
	nb_bins = 1+(table_size*(table_size-1))//2 if (table_size < 11) else None
	
	(n, bins, patches) = the_plot.hist(x, bins = nb_bins,	# density = True,
		align = 'mid', color = the_color.values(), label = the_labels)
	
	if nb_bins is not None:
		
		colisions = [(bins[toto]+bins[toto+1])/2 for toto in range(len(bins)-1)]
		
		the_plot.set_xticks(colisions)
		
		the_plot.set_xticklabels(range(nb_bins))
	
	the_plot.set_xlabel("Nombre de collisions")
	
	the_plot.set_yticks(the_plot.get_yticks())	# Shut the warning up…
	
	the_plot.set_yticklabels(100*the_plot.get_yticks()/len(x))
	
	the_plot.set_ylabel("%")
	
	the_plot.grid(axis = 'y')
	
	the_plot.legend(loc = 'upper right')
	
	the_plot.set_title("table size: "+str(table_size))
	
	the_figure.suptitle("Répartition du nombre de collisions par expérience", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)


def compared_boxes(outputs_path, table_size, num_size, seed, the_color, the_data):
	
	#the_out_path = out_path_builder(outputs_path, table_size, num_size, seed, "boxes")
	
	the_out_path =\
		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "boxes"+"."+fig_extension)
	
	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
	
	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
	
	layout = matplotlib.gridspec.GridSpec(1,1)
	
	the_plot = the_figure.add_subplot(layout[0,0])
	
	
	
	the_figure.suptitle("Répartition du nombre de collisions par expérience", fontsize = 'xx-large')
	
	the_figure.canvas.draw()
	
	# Save it!
	
	the_figure.savefig(the_out_path)


# def compared_total(lis_perm, run_output_path, outputs_path, table_size_list, num_size_list, seed, num_exemple, colors): #TODO: changer étiquette, tester
# 	
# # 	common_in_path = os.path.join(run_output_path, "table size "+str(table_size),
# # 		"num size "+str(num_size), "seed "+str(seed))
# 	
# 	#the_out_path = out_path_builder(outputs_path, table_size_list[0], num_size_list[0], seed, "total")
#	
#	the_out_path =\
#		os.path.join(out_path_builder(outputs_path, table_size, num_size, seed), "total"+"."+fig_extension)
# 	
# 	the_figure = matplotlib.figure.Figure(figsize=(28/inch,19/inch))
# 	
# 	matplotlib.backends.backend_pdf.FigureCanvas(the_figure)
# 	
# 	how_many_perms = len(lis_perm)
# 	
# 	slots = numpy.arange(len(table_size_list))
# 	
# # 	labels = [str(toto) for toto in slots]
# 	
# 	labels =  [str(lenght) for lenght in table_size_list]
# 	
# 	nominal_width = 1/how_many_perms
# 	
# 	layout = matplotlib.gridspec.GridSpec(1,1)
# 	
# 	the_plot = the_figure.add_subplot(layout[0,0])
# 	
# 	for pidx in range(how_many_perms):
# 		
# 		the_perm = lis_perm[pidx]
# 				
# 		total_lis= []
# 		for i,j in zip(table_size_list,num_size_list):
# 			in_folder = in_path_builder(run_output_path,i,j, seed)
# 			with open(os.path.join(in_folder, the_perm+".csv")) as data_file:
# 			
# 				data_csv_reader = csv.reader(data_file, delimiter = ";")
# 			
# 				total_lis.append(total_experience(data_csv_reader, i))
# 						
# 		the_plot.bar(slots+nominal_width*1/2*(pidx-how_many_perms+1)/(how_many_perms-1),
# 			total_lis, nominal_width*1/2, color = colors[pidx], align = 'edge', label = the_perm)
# 	
# 	the_plot.set_xlim([nominal_width/12-1/2, len(table_size_list)+nominal_width/12-1/2])
# 	
# 	the_plot.set_xticks(slots)
# 	
# 	the_plot.set_xticklabels(labels)
# 	
# 	the_plot.set_xlabel("taille de table")
# 	
# 	the_plot.set_ylabel("Nombre de collisions pour cette table")
# 	
# 	the_plot.legend(loc = 'upper left')
# 	
# 	the_plot.grid(axis = 'y')
# 	
# 	the_figure.suptitle("Nombre total de collisions dans l'expérience", fontsize = 'xx-large')
# 	
# 	the_figure.canvas.draw()
# 	
# 	# Save it!
# 	
# 	the_figure.savefig(the_out_path)
