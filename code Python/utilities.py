"""
utilities.py

useful functions for this project
"""

import os


def out_path_builder(outputs_path, table_size, num_size, seed):
	
	# create the subfolder for the given table size
	
	the_out_table_size_path = os.path.join(outputs_path, "table size "+str(table_size))
	
	if not os.path.exists(the_out_table_size_path):
		os.mkdir(the_out_table_size_path)
	
	# create the subsubfolder for a given num size
	
	the_num_size_path = os.path.join(the_out_table_size_path, "num size "+str(num_size))
	
	if not os.path.exists(the_num_size_path):
		os.mkdir(the_num_size_path)
	
	# create the subfolder for a given seed
	
	the_seed_path =  os.path.join(the_num_size_path, "seed "+str(seed))
	
	if not os.path.exists(the_seed_path):
		os.mkdir(the_seed_path)
	
	return the_seed_path
