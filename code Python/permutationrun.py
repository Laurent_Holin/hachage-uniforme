"""
permutationrun.py

Invoque with: python3 path_to_this_script config …


Positional (mandatory) argument:

config:			name of the configuration file


Keyword (optional) arguments:

num_size:		size of the number to be added to the table

sample_size:	size of the sample, 100 by default

seed:			value of the seed for reproductibilty, None by default

Running some experiments on the different permutation to gather data for further empirical
analysis.
"""

import os
import sys
import argparse
import random
import secrets	
import copy
import math
import configparser
import functools

import pandas

import permutations
import openaddr
from utilities import *


# Constants definitions

outputs_top_storage_name = "Outputs"

pickles_storage_name = "Pickles"

output_exention = "csv"

pickle_exention = "pkl"


# CLI exploitation

common_path = os.path.dirname(sys.argv[0])

this_script_name = os.path.splitext(os.path.basename(sys.argv[0]))[0]

parser = argparse.ArgumentParser(description = "Running some experiments on the different"
	+"permutation to gather data for further empirical analysis.")

parser.add_argument("config", help = "name of the configuration file",
	type = str, nargs=1)

parser.add_argument("--num_size", help = "size of the number to be added to the table,"
	+"table_size! by default", type = int, nargs='?', default = None)

parser.add_argument("--sample_size", help = "size of the sample, 100 by default",
	type = int, nargs='?', default = 100)

parser.add_argument("--seed", help = "value of the seed for reproductibilty, None by default",
	type = int, nargs='?', default = None)

args = parser.parse_args()

configuration_file = args.config[0]

sample_size = args.sample_size

seed = args.seed
if  seed == None:
	seed = secrets.randbits(64)

random.seed(seed)


# Check output storage, and perhaps create it

project_script_outputs_path = os.path.join(common_path, outputs_top_storage_name)

if not os.path.exists(project_script_outputs_path):
	os.mkdir(project_script_outputs_path)

script_outputs_path = os.path.join(project_script_outputs_path, this_script_name)

if not os.path.exists(script_outputs_path):
	os.mkdir(script_outputs_path)


# Prepare pickles storage

pickles_storage_path = os.path.join(common_path, pickles_storage_name)

if not os.path.exists(pickles_storage_path):
	os.mkdir(pickles_storage_path)


# Schmurf

# Get the Configuration

configurator = configparser.ConfigParser()

configurator.read(os.path.join(common_path, configuration_file))

# Iterate over the table size

for table_size_lit in configurator.sections():
	
	random.seed(seed) #reinitialising the seed for each run
	#(for each table size, as the number are shared among permutations for the same table size)
	
	table_size = int(table_size_lit)
	
	num_size = math.factorial(table_size) if (args.num_size is None) else args.num_size
	
	seed_folder_path =\
		out_path_builder(script_outputs_path, table_size, num_size,seed )
	
	pickled_seed_folder_path =\
		out_path_builder(pickles_storage_path, table_size, num_size,seed )
	
	# build the experience dictionary
	
	experience = {}
	more = {}
	
	for a_method in configurator[table_size_lit]:
		
		if		a_method == "double_hash_power_mod":
			
			experience[a_method] = getattr(permutations, a_method)
			more[a_method] = None
		
		elif a_method == "double_hash_prime_mod":
			
			experience[a_method] = getattr(permutations, a_method)
			more[a_method] = None
		
		elif	a_method == "permu_factorial_factor":
			
			experience[a_method] = getattr(permutations, a_method)
			more[a_method] = None
		
		elif a_method == "inverted_partial_permu_factorial_factor":
			
			values = configurator[table_size_lit][a_method].split()
			
			experience[a_method] =\
				functools.partial(getattr(permutations, a_method),
				int(values[1]), getattr(permutations, values[0]))
			
			more[a_method] = configurator[table_size_lit][a_method]
		
		else:
			
			raise("Unimplementend method: "+a_method)
	
	# Just do it!
	
	all_data = {}
	all_collisions = {a_method: {} for a_method in experience}
	
	for i in range(sample_size):
		
		all_data[i] = [random.randrange(num_size) for j in range(table_size)]
		
		for a_method in experience:
			
			inserter = openaddr.shredder(table_size, experience[a_method])
			
			all_collisions[a_method][i] = inserter.insert_data(all_data[i])
	
	all_data_frame = pandas.DataFrame.from_dict(all_data, orient = 'index')
	
	all_data_frame.to_csv(os.path.join(seed_folder_path, "elements."+output_exention),
		index = False, header = False, sep = ';')
	
	all_data_frame.to_pickle(os.path.join(pickled_seed_folder_path, "elements."+pickle_exention))
	
	for a_method in experience:
		
		experience_collisions_frame =\
			pandas.DataFrame.from_dict(all_collisions[a_method], orient = 'index')
		
		output_name = a_method+("+("+more[a_method]+")" if more[a_method] is not None else "")
		
		experience_collisions_frame.to_csv(os.path.join(seed_folder_path, output_name+"."+output_exention),
			index = False, header = False, sep = ';')
		
		experience_collisions_frame.to_pickle(os.path.join(pickled_seed_folder_path, output_name+"."+pickle_exention))


# In the end, there can be None

sys.exit(0)
