"""
openaddr.py

Lean open addressing hash table library to work with the permutation library

"""
#import functools


class shredder:
	
	def __init__(self, storage_size, hash_function):
		"""
		Create an empty hash table, with a size of storage_size and using the
		hash_function as hash function.
		"""
		
		self.size = storage_size
		
		self.storage = [None]*self.size
		
		self.grinder = hash_function
	
	
	def insert_data(self, data):
		
		if len(data) > self.size:
			
			raise(RuntimeError("Too many data for this table!"))
		
		collisions = []
		
		for a_datum in data:
			
			permu = self.grinder(a_datum, self.size)
			
			for tries, slot in enumerate(permu):
				
				if self.storage[slot] is None:
					
					self.storage[slot] = a_datum
					collisions.append(tries)
					break
		
		return collisions

